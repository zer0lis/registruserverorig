<?php

class main_model extends CI_Model {

	function __construct() {
        parent::__construct();
        
        $this->load->database();
    }

    public function get_records($table, $idzi) {
    	$this->db->where('IDzi', $idzi);
		$this->db->select('*');
		$result = $this->db->get($table)->result_array();

		$count = count($result);
		
		for($i = 0; $i < $count; $i++) {
			$result[$i]['ID'] = intval($result[$i]['ID']);
			$result[$i]['IDZi'] = intval($result[$i]['IDZi']);
			$result[$i]['Suma'] = floatval($result[$i]['Suma']);
			if($table != 'SumeAport') $result[$i]['IDFurnizor'] = intval($result[$i]['IDFurnizor']);
		}
		
		return $result;	
    }

    public function new_record($table, $data) {
         $this->db->insert($table, $data);
	}

	public function edit_record($table, $id, $data) {			
		$this->db->where('ID', $id);
		$this->db->update($table, $data);	
	}

	public function delete_record($table, $id) {
		$this->db->where('id', $id);
		$this->db->delete($table);
	}

    public function __destruct() {
        $this->db->close();
    }

}
