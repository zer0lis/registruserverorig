<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {

	public function __construct() {

          parent::__construct();
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the model classes and helpers
          $this->load->model(['user_model', 'main_model', 'soldinitial_model', 'furnizori_model', 'date_model', 'calcul_model']);
          $this->load->helper('registru_helper'); 
 
     }

     public function index() {
          $last_day = $this->date_model->last_day_id();
          
          $data['server_data'] = json_encode($this->get_records($last_day)); 

          $this->load->view('index',$data);

     }

     //for ajax requests
     public function get_records_json($idzi) {
          echo json_encode($this->get_records($idzi));
     }

     public function test() {
          print_r($this->furnizori_model->get_furnizori(1));
     }


     public function get_records($idzi) {
          $chelt = $this->main_model->get_records('SumeCheltuieli', $idzi);
          $marfa9 = $this->main_model->get_records('SumeMarfaTVA9', $idzi);
          $marfa24 = $this->main_model->get_records('SumeMarfaTVA24', $idzi);
          $aport = $this->main_model->get_records('SumeAport', $idzi);
          $soldinitial = $this->sold_initial($idzi);
          $zi = $this->date_model->get_id_date_by_id($idzi);
          $furnizori = $this->get_furnizori();
          
          return ['cheltuieli' => $chelt, 'marfa9' => $marfa9, 'marfa24' => $marfa24, 'aport' => $aport, 'sold_initial' => $soldinitial,'zi' => $zi, 'furnizori' => $furnizori] ;
     }

     function get_furnizori() {
          $furnizori_chelt = $this->furnizori_model->get_furnizori(1);
          $furnizori_marfa9 = $this->furnizori_model->get_furnizori(2);
          $furnizori_marfa24 = $this->furnizori_model->get_furnizori(3);

          return ['furnizori_chelt' => $furnizori_chelt, 'furnizori_marfa9' => $furnizori_marfa9, 'furnizori_marfa24' => $furnizori_marfa24];
     }

     public function new_day($idzi) {
          if(last_day_id() == $this->id_first_day_of_month($idzi)) {
               
          }

          $this->get_records($this->date_model->last_day_id());
          
     }

     public function id_first_day_of_month($idzi) {
          $year = $this->date_model->get_date_by_id($idzi)['year'];
          $month = $this->date_model->get_date_by_id($idzi)['month'];

          $id_of_first_day = intval($this->date_model->id_first_day($year, $month));
          
          return $id_of_first_day;
          
     }

     public function sold_initial($idzi) {
          $this->soldinitial_model->get_sold_initial($idzi);
     }

}